'''
To generate the GF(7) multiplication table the numbers for the row and column are multiplied

and then applied to modulus 7.

Similiar process for addition except we add the numbers for the row and column.
'''



rows, columns = (7, 7)

gf_product_table = [[0 for i in range(columns)] for j in range(rows)]

gf_addition_table = [[0 for i in range(columns)] for j in range(rows)]

i = 0

j = 0

while i < rows:
    
    j = 0

    while j < columns:
        
        gf_product_table[i][j] = ( i * j ) % 7

        j += 1

    i += 1

i = 0

j = 0

while i < rows:
    
    j = 0

    while j < columns:
        
        gf_addition_table[i][j] = ( i + j ) % 7

        j += 1

    i += 1

print("Printing GF(7) Multiplication Table:")

print("x*y\t0\t1\t2\t3\t4\t5\t6")

i = 0

j = 0

while i < rows:
    
    j = 0

    print(str(i),end='')

    while j < columns:
        
        print("\t",end='')

        print(str(gf_product_table[i][j]),end='')

        j += 1

    print('')

    i += 1


print("Printing GF(7) Addition Table:")

print("x+y\t0\t1\t2\t3\t4\t5\t6")

i = 0

j = 0

while i < rows:
    
    j = 0

    print(str(i),end='')

    while j < columns:
        
        print("\t",end='')

        print(str(gf_addition_table[i][j]),end='')

        j += 1

    print('')

    i += 1
