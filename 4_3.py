'''
This program does Galois Field multiplication for GF(8)

based on the StackOverflow Post:

    https://stackoverflow.com/questions/13202758/multiplying-two-polynomials
'''

'''
The below program does the Polynomial multiplication

of polynomials a and b where the polynomial is

represented as a binary number (e.g. x^2 + x + 1

means the binary number 0b111  = 7)
'''
def poly_mult(a,b):

    i = 0

    c = 0

    while b > 0:
        
        if b & 0b1 == 1:

            c ^= ( a << i )

        b >>= 1

        i += 1

    return c

def find_degree(m):

    degree = 0
    
    i = 0

    while m > 0:

        if m & 0b1 == 1:
            
            degree = i

        m >>= 1
        
        i += 1

    return degree


def poly_mod(a,b,m):
    
    c = poly_mult(a,b)

    result = 0

    c_init = c

    degree = find_degree(m)

    i = 0

    while c > 0:
        
        if i < degree and (c & 0b1 == 0b1):

            result ^= (0b1 << i)

        elif i == degree and (c & 0b1 == 0b1):

            result ^=  ( ( 0b1 << degree ) ^ ( m ) )

        elif i > degree and ( c & 0b1 == 0b1): 
            
            result ^=  ( ( ( 0b1 << degree ) ^ m ) << ( i - degree ) )
        
        c >>= 1

        i += 1

    return result

def print_gf_table(gf_number,poly_binary):

    rows, cols = (gf_number, gf_number)
    
    gf_table = [[0 for i in range(cols)] for j in range(rows)]

    i = 0

    j = 0

    while i < rows:

        j = 0

        while j < cols:

            gf_table[i][j] = poly_mod(i,j,poly_binary)

            j += 1

        i += 1

    i = 0

    j = 0

    while i < rows:

        j = 0

        while j < cols:
            
            print('\t',end='')

            print(gf_table[i][j],end='')

            j += 1

        print('')

        i += 1

print_gf_table(8,11)

print('')

print("0 means 0 as a Polynomial")

print("1 means 1 as a Polynomial")

print("2 means x as a Polynomial")

print("3 means x + 1 as a Polynomial")

print("4 means x^2 as a Polynomial")

print("5 means x^2 + 1 as a Polynomial")

print("6 means x^2 + x^1 as a Polynomial")

print("7 means x^2 + x^1 + 1 as a Polynomial")

