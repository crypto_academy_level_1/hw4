'''
This program does Galois Field multiplication for GF(8)

is based on the algorithm found in the Wikipedia article:

    https://en.wikipedia.org/wiki/Finite_field_arithmetic
'''

'''
Both and and b are unsigned 8-bit

integers.
'''
def poly_mult_aes(a,b):
    
    product = 0

    carry = 0

    while a > 0 and b > 0:

        if b & 0b1 == 0b1:
            
            product ^= a
        
        if a & 0x80 > 0:

            a = (a << 1) ^ 0x11b

        else:

            a <<= 1

        b >>= 1

    return product


print(poly_mult_aes(0x29,0x0A))

print(poly_mult_aes(0xF3,0x34))

print(poly_mult_aes(0x01,0x01))
